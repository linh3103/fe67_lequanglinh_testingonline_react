import React, { Component } from 'react';

class TextField extends Component {
    
    render() {
        const question = this.props.question
        return (
            <div>
                <h5>Câu {question.id}: {question.content}</h5>
                <input onChange={this.handleChange} id={question.id} type="text" className="form-control my-2" style={{width: '60%'}}/>
            </div>
        );
    }
}

export default TextField;