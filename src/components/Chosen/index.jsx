import React, { Component } from 'react';

class Chosen extends Component {
    render() {
        const question = this.props.question
        return (
            <div>
                <h5>Câu {question.id}: {question.content}</h5>
                {
                    question.answers.map((item) => {
                        return (
                            <div key={item.id}>
                                <input type="radio" id={`${question.id}`} name={`${question.id}`}/>
                                &nbsp; 
                                <label style={{fontSize: 18}} htmlFor={`${question.id}`}>
                                    {item.content}
                                </label>
                            </div>

                        )
                    })
                }
            </div>
        );
    }
}

export default Chosen;