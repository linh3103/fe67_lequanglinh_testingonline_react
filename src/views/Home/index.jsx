import React, { Component } from 'react';
import { connect } from 'react-redux';
import Chosen from '../../components/Chosen';
import TextField from '../../components/TextField';
import { fetchQuestions } from '../../store/actions/question';

class Home extends Component {
    render() {
        return (
            <div>
                <div className="container">
                <h1 className="text-center my-3">Online test</h1>
                <form>
                    {
                        this.props.questionList.map((item) => {
                            return (
                                item.questionType === 1 ? 
                                <Chosen key={item.id} question={item}/> : 
                                <TextField key={item.id} question={item}/>
                            )
                        })
                    }
                    <button type="submit" className="btn btn-primary my-3">
                        Nộp bài
                    </button>
                </form>
                </div>
            </div>
        );
    }
    componentDidMount(){
        this.props.dispatch(fetchQuestions)
    }
}

const mapStateToProps = (state) =>{
    return{
        questionList: state.questions.questionList,
    }
}

export default connect(mapStateToProps) (Home);