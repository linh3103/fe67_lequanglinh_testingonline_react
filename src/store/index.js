import { applyMiddleware, combineReducers, compose, createStore } from "redux";
import thunk from "redux-thunk";
import questions from './reducers/questionReducer';
import answers from './reducers/answerReducer'
const reducer = combineReducers({
    questions,
    answers
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore (
    reducer,
    composeEnhancers(applyMiddleware(thunk))
)