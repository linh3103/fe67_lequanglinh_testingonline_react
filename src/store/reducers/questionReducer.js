const initialState = {
    questionList: []
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case "FETCH_QUESTIONS":
            state.questionList = action.payload
            return {...state};
    
        default:
            return state;
    }
}

export default reducer